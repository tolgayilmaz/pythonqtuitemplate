@echo OFF
D:\Apps\Anaconda3\Scripts\activate.bat D:\Apps\Anaconda3

SET QT_PLUGIN_PATH = D:\Apps\Anaconda3\Lib\site-packages\PyQt5\Qt\bin

PyInstaller --onefile --noupx --noconsole --clean --paths ui;src ^
--icon=ui/images/app.ico ^
--hidden-import PyQt5.sip ^
--hidden-import ui ^
--hidden-import pandas._libs.tslibs.timedeltas ^
--hidden-import pandas._libs.tslibs.nattype ^
--hidden-import pandas._libs.tslibs.np_datetime ^
--hidden-import pandas._libs.skiplist ^
--exclude matplotlib ^
--exclude scipy ^
--exclude numpy.py ^
--exclude mkl ^
src/MainWindowView.py -n TemplateApp
PAUSE

--paths D:\Apps\Anaconda3\Lib\site-packages\PyQt5\Qt\bin
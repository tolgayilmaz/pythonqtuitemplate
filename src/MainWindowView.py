import sys
sys.path.append(".")
sys.path.append("..")
import pandas as pd
import numpy as np
#from sklearn.svm import SVC

from PyQt5 import QtWidgets
import ConfManager
from ui.MainWindow import Ui_MainWindow


class MainWindowView(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindowView, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.conf = ConfManager.ConfManager()


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindowView()
    window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
from configparser import ConfigParser, RawConfigParser, NoOptionError
import os

dir_path = os.path.dirname(os.path.realpath(__file__))


class ConfManager(object):
    def __init__(self):
        self.config = RawConfigParser()
        self.conf_file = os.path.join(dir_path, '../resources/conf.ini')
        self.config.read(self.conf_file)
        self.load_defaults()
        self.load_conf()

    def load_defaults(self):
        None

    def load_conf(self):

        try:
            None
        except Exception as err:
            print_exception()

    def save_paths(self):
        with open(self.conf_file, 'w+') as configfile:
            self.config.write(configfile)

    def save_conf(self):
        with open(self.conf_file, 'w+') as configfile:
            self.config.write(configfile)


def extract_relative_path(path):
        index = path.find('..')
        return path[index+1 if index == -1 else index:]

def print_exception():
    import linecache
    import sys
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))